import 'package:flutter/material.dart';
import 'package:profile_page/main.dart';

enum APP_THEME{LIGHT,DARK}


void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.indigo.shade500,
        )
    );
  }

  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
        iconTheme: IconThemeData(
          color: Colors.white60,
        )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}
class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child:
          Icon(Icons.threesixty),
          onPressed: (){
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
        ),
      );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
       //   color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("095-913-4526"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
     // color: Colors.indigo.shade500,
      onPressed: (){},
    ),
  );
}

Widget mobilePhoneListTile2(){
  return ListTile(
    leading: Text(""),
    title: Text("061-852-0214"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
     // color: Colors.indigo.shade500,
      onPressed: (){},
    ),
  );
}

Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("warin.rung27@gamil.com"),
    subtitle: Text("work"),
  );
}

Widget locationListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St, Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      //color: Colors.indigo.shade500,
      onPressed: (){},
    ),
  );
}

AppBar buildAppBarWidget(){
  return AppBar(
    backgroundColor: Colors.blueGrey[900],
    leading: Icon(
      Icons.arrow_back,
      color: Colors.white,
    ),

    actions: <Widget>[
      IconButton(onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.white,
      )
    ],
  );
}

ListView buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 250,
            child: Image.network(
              "https://cdn.discordapp.com/attachments/866545462738550824/1054047028428157048/IMG_3527.jpg"
              ,fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text("Warinrata Ritdech",
                      style: TextStyle(fontSize:30),
                    )

                )
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data : ThemeData(
                iconTheme: IconThemeData(
                  color: Colors.pink,
                ),
              ),
                child: profileActionItems(),
            ),
          ),
          mobilePhoneListTile(),
          mobilePhoneListTile2(),
          emailListTile(),
          locationListTile()
        ],
      ),
    ],

  );
}

Widget profileActionItems(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
      buildCallButton(),buildTextButton(),
  buildVideoButton(),buildEmailButton(),
  buildDirectionsButton(),buildPayButton(),
      ],
  );
}

